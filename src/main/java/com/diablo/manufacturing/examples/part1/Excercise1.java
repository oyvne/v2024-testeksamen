package com.diablo.manufacturing.examples.part1;

import java.util.ArrayList;
import java.util.Collection;

public class Excercise1 {
    
    /**
     * Study the class. Assume there are more methods,
     * but no method involves casting foo.
     * 
     * Comment out the line(s) that are true.
     */
    Collection<String> foo = new ArrayList<>();

    // Assume methods

    public void on_the_specter() {
        // TODO Uncomment the line(s) that are true

        // I_can_use_foo_append();
        // I_can_use_foo_get();
        // foo_cannot_be_iterated();
        // Changing_foo_to_class_LinkedList_gives_similar_behaviour();
    }
}
